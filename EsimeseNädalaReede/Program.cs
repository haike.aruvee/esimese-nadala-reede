﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsimeseNädalaReede
{
    class Program
    {
        static void Main(string[] args)
        {
            double arv = 77;
            arv += Math.Pow(7, 3);
            Console.WriteLine(arv);

            Console.WriteLine("Mis su nimi on: ");
            var name = Console.ReadLine(); // lahendamata, et kui nimi on mitme osaline
            var nimi = name.ToLower();
            Console.WriteLine(char.ToUpper(nimi[0]) + nimi.Substring(1));

            // Hennu lahendus reede hommiku repos

            int[] massiiv = new int[10]; //massiiv mis koosneb kümnest elemendist
            int[] teineMassiiv = massiiv; //sama mis eelmine
            int[] kolmasMassiiv = { 1, 2, 3, 4 }; // see on 4 elemendiline massiiv, mis sisaldab arve 1-4
            int[] neljasMassiiv;
            foreach (var x in kolmasMassiiv) Console.WriteLine(x);

            List<int> arvud = new List<int>();

            Random r = new Random();
            int x = r.Next(100);

            List<int> kaardid = Enumerable.Range(0, 52).ToList();
            for (int i = 0; i < kaardid.Count; i++)
                Console.Write($"t{kaardid[i]} {(i % 4 == 3 ? "\n" : "")}");

            List<int> segatudpakk = new List<int>();
            while (kaardid.Count > 0)
            { }






        }
    }
}

